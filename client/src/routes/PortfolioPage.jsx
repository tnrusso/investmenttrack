import { Link, useNavigate, useParams, Navigate } from "react-router-dom";
import { useEffect, useState, useLayoutEffect } from "react";
import userService from "../services/user.service";
import stockService from '../services/stock.service';
import portfolioService from '../services/portfolio.service';
import { Logout } from "../components/Logout";
import { Button, Table, Image, Dropdown } from 'react-bootstrap';
import { AddStock } from "../components/AddStock";
import { SellStock } from "../components/SellStock";
import { Login } from "../components/Login";
import { TopNavbar } from "../components/TopNavbar";
import logo from '../images/logo.png';
import { TransactionsTable } from "../components/TransactionsTable";
import { SharesTable } from "../components/SharesTable";

export function PortfolioPage(props) {
    const [portfolio, setPortfolio] = useState({});
    const [amountSpent, setAmountSpent] = useState(0);
    const [getUpdate, setGetUpdate] = useState();
    const [portfolios, setPortfolios] = useState([{}])

    let navigate = useNavigate();
    let params = useParams();
    let user_id = window.localStorage.getItem("loggedIn");
    // console.log(portfolio)

    // Load the portfolio
    useEffect(() => {
        // console.log(getUpdate)
        portfolioService.getPortfolio(params.portfolio_id)
            .then(response => {
                setPortfolio(response.data);
            })
            .catch(error => {
                // console.log('something went wrong', error);
            })
    }, [getUpdate, setGetUpdate, params])

    // Get list of portfolios
    useEffect(() => {
        async function getPortfolioData() {
            portfolioService.getUserPortfolios(user_id)
                .then(response => {
                    setPortfolios(response.data);
                })
                .catch(error => {
                    console.log('something went wrong', error);
                })
        }

        if (window.localStorage.getItem("loggedIn")) {
            getPortfolioData();
        }
    }, [window.localStorage.getItem("loggedIn")])

    function changePortfolio(p_id) {
        navigate("/portfolio/" + p_id)
    }

    return (
        <div id='portfolio-container'>
            {
                (window.localStorage.getItem("loggedIn"))
                    ?
                    <>
                    <TopNavbar />
                        <div className="portfolio-info">
                            <Image src={logo} fluid={true} onClick={() => navigate("/")} />
                            <div className="account-info-box">
                                <p>Name: {portfolio.name}</p>
                                <p>Initial Amount: ${(portfolio.amount)}</p>
                                <p>Amount Spent: ${(amountSpent).toFixed(2)}</p>
                                <p>Available Funds: ${(portfolio.available_amount * 1.0).toFixed(2)}</p>
                            </div>
                        </div>
                        <Dropdown>
                            <Dropdown.Toggle variant="success">
                                Change Portfolio
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {portfolios.map((portfolio, index) => {
                                    return (
                                        <Dropdown.Item key={index} onClick={() => changePortfolio(portfolio.id)}>{portfolio.name}</Dropdown.Item>
                                    );
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                        <hr />
                        <h2>Current Shares</h2>
                        {/* <AddStock portfolio_id={params.portfolio_id} newStock={setNewStock} amount={portfolio.available_amount} /> */}
                        <br />
                        <SharesTable
                            user_id={user_id} portfolio_id={params.portfolio_id}
                            setAmountSpent={setAmountSpent} getUpdate={setGetUpdate} portfolio={portfolio}
                        />
                        <br />
                        <hr />
                        <br /><br />
                        <TransactionsTable portfolio_id={params.portfolio_id} user_id={user_id} />

                    </>
                    :
                    <Navigate to="/" />
            }
        </div>
    )
}