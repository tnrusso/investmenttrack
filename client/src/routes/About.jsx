import { useEffect, useState } from 'react';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { Button, Table, Image, Form, Row, Col, Container } from 'react-bootstrap';
import { TopNavbar } from "../components/TopNavbar";
import userService from '../services/user.service';
import logo from '../images/logo.png'
import portfoliopage from '../images/portfoliopage.png'
import stockpage from '../images/stockpage.png'

export function About() {

    let navigate = useNavigate();

    return (
        <div id="about-page" style={{ width: "100%", height: "100%", minHeight: "100vh" }}>
            <TopNavbar />
            <div id='about-content'>
                <Image src={logo} fluid={true} />
                <br />
                <div className="divider-container">
                    <span className="divider-left" />
                    <span className="divider" />
                    <span className="divider-right" />
                </div>
                <p>
                    Investment Track is a web application that allows for users to track their personal hypothetical stock market portfolios.
                    <br></br><br></br>
                    After creating an account, you will be able to create your own personal stock portfolios, choose which stocks you want to invest in, and track your gains and losses over time.
                    <br /><br />
                    Perfect for those who are looking to learn about trading in the stock market without the risk of losing money!
                    <br></br>
                </p>
                <div className="divider-container">
                    <span className="divider-left" />
                    <span className="divider" />
                    <span className="divider-right" />
                </div>
                <br />
                <Container>
                    <Row>
                        <Col xs={12} lg={6} md={6} sm={12} xl={6} className="project-card-col center">
                            <Image src={portfoliopage} fluid={true} thumbnail className='about-img' />
                        </Col>

                        <Col xs={12} lg={6} md={6} sm={12} xl={6} className="project-card-col center">
                            <Image src={stockpage} fluid={true} thumbnail className='about-img' />
                        </Col>
                    </Row>
                </Container>
                <br />
                <div className="divider-container">
                    <span className="divider-left" />
                    <span className="divider" />
                    <span className="divider-right" />
                </div>
                <br /><br />
                <p>
                    Don't want to register? Use these login details for a quick demonstration!
                    <br />
                    <span><strong>User ID: </strong> user123</span>
                    <span><strong>Password: </strong> pass123</span>
                </p>
            </div>
        </div>
    );
}