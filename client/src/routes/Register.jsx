import { useEffect, useState } from 'react';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { Button, Table, Image, Form } from 'react-bootstrap';
import { TopNavbar } from '../components/TopNavbar';
import userService from '../services/user.service';
import logo from '../images/logo.png'

export function Register(props) {
    const [user, setUser] = useState("");
    const [pass, setPass] = useState("");
    const [errorMessage, setErrorMessage] = useState(false);

    let navigate = useNavigate();

    function handleUser(e) {
        setUser(e.target.value);
    }

    function handlePass(e) {
        setPass(e.target.value);
    }

    function handleSubmit() {
        const info = { "name": user, "password": pass }
        userService.create(info)
            .then(response => {
                // console.log("user added successfully", response.data);
                setErrorMessage(false);
                navigate("/");
            })
            .catch(error => {
                setErrorMessage(true);
                // console.log('something went wroing', error);
            })
    }

    return (
        <>
            {
                (window.localStorage.getItem("loggedIn"))
                    ?
                    <Navigate to="/" />
                    :
                    <>
                        <div className="sign-in-container">
                            <TopNavbar />
                            <div className='sign-in-box'>
                                <Image src={logo} fluid={true} onClick={() => navigate("/")} />
                                <br /><br />
                                <div id='signup-login-box'>
                                    <Form>
                                        <h1>Sign up</h1>
                                        <Form.Group className="mb-3" controlId="userid">
                                            <Form.Label>User ID</Form.Label>
                                            <Form.Control type="text" placeholder="Enter User ID" onChange={handleUser} />
                                        </Form.Group>

                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type="password" placeholder="Password" onChange={handlePass} />
                                        </Form.Group>
                                        {(errorMessage
                                            ?
                                            <p className='error-message'>Username not available</p>
                                            :
                                            <></>
                                        )}
                                        <span className='even-btns'>
                                            <Button variant='primary' onClick={handleSubmit}>Register</Button>
                                        </span>
                                    </Form>
                                    <br />
                                    Already have an account? <a href='/'>Login here</a>
                                </div>
                            </div>
                        </div>
                    </>
            }
        </>

    );
}