import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import userService from "../services/user.service";
import stockService from '../services/stock.service';
import portfolioService from '../services/portfolio.service';
import { Login } from "../components/Login";
import { Logout } from "../components/Logout";
import { CreatePortfolio } from "../components/CreatePortfolio";
import { PortfolioListItem } from "../components/PortfolioListItem";
import { TopNavbar } from "../components/TopNavbar";
import { Button, Image } from 'react-bootstrap';
import { AddStock } from "../components/AddStock";
import logo from '../images/logo.png';

export function Main() {
    const [user, setUser] = useState({})
    const [portfolios, setPortfolios] = useState([{}])
    const [newPortfolio, setNewPortfolio] = useState("");

    let navigate = useNavigate();


    useEffect(() => {
        async function getPortfolioData() {
            let user_id = window.localStorage.getItem("loggedIn")
            portfolioService.getUserPortfolios(user_id)
                .then(response => {
                    // console.log(response.data);
                    setPortfolios(response.data);
                })
                .catch(error => {
                    console.log('something went wrong', error);
                })
        }

        if (window.localStorage.getItem("loggedIn")) {
            getPortfolioData();
        }
    }, [window.localStorage.getItem("loggedIn"), newPortfolio])

    return (
        <>
            {
                (window.localStorage.getItem("loggedIn") || Object.keys(user).length != 0)
                    ?
                    <div id='main'>
                        <TopNavbar />
                        <div id="main-content">
                            <Image src={logo} fluid={true} />
                            <br />
                            <CreatePortfolio setNewPortfolio={setNewPortfolio} />
                            <div className="divider-container">
                                <span className="divider-left" />
                                <span className="divider" />
                                <span className="divider-right" />
                            </div>
                            <br />
                            <h2>Portfolios</h2>
                            <br />
                            {portfolios.map((portfolio, index) => {
                                return (
                                    <PortfolioListItem key={index} portfolio_id={portfolio.id} name={portfolio.name} amount={portfolio.amount} user_id={portfolio.user_id} />
                                );
                            })}
                            <br />
                            <div className="divider-container">
                                <span className="divider-left" />
                                <span className="divider" />
                                <span className="divider-right" />
                            </div>
                            <br />
                            <Logout setUser={setUser} />
                        </div>
                    </div>
                    :
                    <div style={{ width: "100%" }} >
                        <TopNavbar />
                        <Login setUser={setUser} />
                    </div >
            }
        </>
    )
}