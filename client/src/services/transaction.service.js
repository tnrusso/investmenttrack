import httpClient from "../http-common";

const getUserTransactions = (user_id, portfolio_id) => {
    return httpClient.get(`/userTransactions/${user_id}/${portfolio_id}`);
}

const saveTransaction = data => {
    return httpClient.post("/saveTransaction", data);
}

export default { getUserTransactions, saveTransaction };