import httpClient from "../http-common";

const getData = (symbol) => {
    return httpClient.get(`/stock/${symbol}`);
}

const getUserStocks = (user_id, portfolio_id) => {
    return httpClient.get(`/userStocks/${user_id}/${portfolio_id}`);
}

const saveStock = data => {
    return httpClient.post("/saveStock", data);
}

const sellStock = data => {
    return httpClient.put("/sellStock", data);
}

const sellAllStock = id => {
    return httpClient.delete(`/sellAllStock/${id}`, id);
}

export default { getData, saveStock, getUserStocks, sellStock, sellAllStock };