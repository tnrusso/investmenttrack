import httpClient from "../http-common";

const getPortfolio = id => {
    return httpClient.get(`/portfolio/${id}`);
}

const getUserPortfolios = user_id => {
    return httpClient.get(`/userPortfolios/${user_id}`);
}

const savePortfolio = data => {
    return httpClient.post("/savePortfolio", data);
}

const updatePortfolio = data => {
    return httpClient.put("/updatePortfolio", data);
}

export default { getPortfolio, getUserPortfolios, savePortfolio, updatePortfolio };