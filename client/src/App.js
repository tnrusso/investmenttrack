import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";
import { NotFound } from './components/NotFound';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Main } from './routes/Main';
import { Register } from './routes/Register';
import { About } from './routes/About';
import "./App.css";
import { PortfolioPage } from "./routes/PortfolioPage";

function App() {
  return (
    <div id='app'>
        <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<Main />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/about" element={<About />} />
            <Route exact path="/portfolio/:portfolio_id" element={<PortfolioPage />} />
            <Route path="*" element={<Main />} />
          </Routes>
        </BrowserRouter>
    </div>
  );
}


export default App;