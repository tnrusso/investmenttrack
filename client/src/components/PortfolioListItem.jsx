import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import userService from '../services/user.service';
import portfolioService from '../services/portfolio.service';
import { Button, Modal, Form } from 'react-bootstrap';

export function PortfolioListItem(props) {

    let navigate = useNavigate();

    function openPortfolio() {
        navigate("/portfolio/" + props.portfolio_id)
    }

    return (
        <>
            <div className='portfolio-tab' onClick={openPortfolio}>
                <span><h5>{props.name}</h5></span>
                <span><h6>Initial Investment: ${props.amount}</h6></span>
            </div>
            <br />
        </>
    );
}