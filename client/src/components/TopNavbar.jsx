import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { Button, Image, Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import logo2 from '../images/logo2.png';

export function TopNavbar() {
    let navigate = useNavigate();

    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top">
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            alt=""
                            src={logo2}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}
                        Investment Track
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                        <Nav className="me-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            {
                                (window.localStorage.getItem("loggedIn"))
                                    ?
                                    <>
                                    </>
                                    :
                                    <>
                                        <Nav.Link href="/register">Register</Nav.Link>
                                    </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}