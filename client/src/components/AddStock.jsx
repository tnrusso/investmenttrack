import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import userService from '../services/user.service';
import stockService from '../services/stock.service';
import portfolioService from '../services/portfolio.service';
import transactionService from '../services/transaction.service';
import { Button, Modal, Form } from 'react-bootstrap';

export function AddStock(props) {
    const [show, setShow] = useState(false);
    const [symbol, setSymbol] = useState("");
    const [currentSymbol, setCurrentSymbol] = useState("");
    const [shares, setShares] = useState(0);
    const [price, setPrice] = useState(0);
    const [totalPrice, setTotalPrice] = useState(0);
    const [data, setData] = useState({});

    const handleModalClose = () => setShow(false);
    const handleModalShow = () => setShow(true);

    const [stockData, setStockData] = useState(false);

    async function getStockData() {
        // console.log(symbol);
        setStockData(true);
        stockService.getData(symbol.toUpperCase())
            .then(response => {
                // console.log(response.data);
                setCurrentSymbol(symbol.toUpperCase())
                setData(response.data);
                setPrice(response.data.c);
            })
            .catch(error => {
                // console.log('something went wrong', error);
            })
    }

    function handleChange(e) {
        setSymbol(e.target.value);
    }

    function handleSharesChange(e) {
        let max_shares = (props.portfolio.available_amount / price)
        let max_price = price * max_shares
        if (price * e.target.value < props.portfolio.available_amount) {
            setShares(e.target.value);
            setTotalPrice(price * e.target.value);
        } else {
            setTotalPrice(max_price);
            setShares(max_shares);
        }

    }

    async function saveChanges() {
        var currentdate = new Date().toLocaleString();
        if (totalPrice <= props.portfolio.available_amount && shares > 0) {
            const pInfo = { "id": props.portfolio.id, "user_id": window.localStorage.getItem("loggedIn"), "available_amount": props.portfolio.available_amount - totalPrice, "name": props.portfolio.name, "amount": props.portfolio.amount }
            portfolioService.updatePortfolio(pInfo)
                .then(response => { // Updating Portfolio
                    const info = { "buy_price": data.c, "buydatetime": currentdate, "shares": shares, "symbol": symbol.toUpperCase(), "total_buy_price": totalPrice, "user_id": window.localStorage.getItem("loggedIn"), "portfolio_id": props.portfolio.id }
                    stockService.saveStock(info)
                        .then(response => { // Update Stocks table
                            const tInfo = { "action": "BUY", "datetime": currentdate, "symbol": symbol.toUpperCase(), "amount": shares, "price": data.c, "portfolio_id": props.portfolio.id, "user_id": window.localStorage.getItem("loggedIn") }
                            transactionService.saveTransaction(tInfo)
                                .then(res => { // Updating transactions table
                                    props.setGetUpdate(new Date().getMilliseconds())
                                })
                                .catch(err => { })
                        })
                        .catch(error => {
                            // console.log('something went wroing', error);
                        })
                })
                .catch(error => {
                })

            setStockData(false);
            setPrice(0);
            setTotalPrice(0);
            setData({});
            setCurrentSymbol("");
            setShares(0);
            handleModalClose();
        } else {

        }
    }

    return (
        <>
            <Button variant="primary" onClick={handleModalShow}>
                Add Stock
            </Button>

            <Modal show={show} onHide={handleModalClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a stock to your portfolio</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formSymbol">
                            <Form.Label>Stock symbol</Form.Label>
                            <Form.Control type="text" placeholder="Enter stock symbol" value={symbol} onChange={handleChange} />
                            <Form.Text className="text-muted">
                                Example: AAPL, AMZN, GOOGL
                            </Form.Text>
                        </Form.Group>
                        <Button variant="primary" onClick={getStockData}>
                            Search
                        </Button>
                        <br /><br />
                        {stockData
                            ?
                            <>
                                <hr />
                                <h4>{currentSymbol.toUpperCase()}</h4>
                                <Form.Group className="mb-3" controlId="formShares">
                                    <a target="_blank" rel="noopener noreferrer" href={"https://stocks-watch.herokuapp.com/symbol/" + symbol}>View Stock Information</a>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formShares">
                                    <Form.Label>Number of shares</Form.Label>
                                    <Form.Control type="text" placeholder="Enter number of shares" value={shares} onChange={handleSharesChange} maxLength={10} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formShares">
                                    <Form.Label>Current price: ${price}</Form.Label>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formShares">
                                    <Form.Label>Total cost: ${totalPrice}</Form.Label>
                                </Form.Group>
                            </>
                            :
                            <></>
                        }
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleModalClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={saveChanges}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}