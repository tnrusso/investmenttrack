import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import userService from '../services/user.service';
import stockService from '../services/stock.service';
import transactionService from '../services/transaction.service'
import { Button, Modal, Form, Table, Pagination } from 'react-bootstrap';
import { SellStock } from './SellStock';
import { AddStock } from './AddStock';

export function SharesTable(props) {
    const [amountSpent, setAmountSpent] = useState(0);
    const [stocks, setStocks] = useState([{}]);
    const [prices, setPrices] = useState(new Map());
    const [sold, setSold] = useState(0);
    const [page, setPageIndex] = useState(0);
    const [newStock, setNewStock] = useState();
    const [getUpdate, setGetUpdate] = useState();

    let ROWS_PER_PAGE = 5;

    // Update table data
    useEffect(() => {
        async function getStocks() {
            stockService.getUserStocks(props.user_id, props.portfolio_id)
                .then(response => {
                    let total = 0;
                    response.data.map((stock, index) => {
                        total += stock.total_buy_price;
                        if (!(stock.symbol in prices)) {
                            stockService.getData(stock.symbol)
                                .then(response => {
                                    setPrices(prices => new Map(prices.set(stock.symbol, response.data.c)))
                                })
                                .catch(error => {
                                })
                        }
                    })
                    props.setAmountSpent(total);
                    setStocks(response.data)
                })
                .catch(error => {
                    // console.log('something went wrong', error);
                })
        }
        getStocks();
    }, [props, getUpdate])

    useEffect(() => {
        props.getUpdate(getUpdate)
    }, [getUpdate, setGetUpdate])

    function paginate(params) {
        if (params === 'next') {
            if (page + ROWS_PER_PAGE > stocks.length) {
                setPageIndex(stocks.length - (stocks.length % ROWS_PER_PAGE));
            }
            else if (stocks.length == page + ROWS_PER_PAGE) {
                // Do nothing
            } else {
                setPageIndex(page + ROWS_PER_PAGE)
            }
        } else {
            if (page - ROWS_PER_PAGE >= 0) {
                setPageIndex(page - ROWS_PER_PAGE)
            }
        }
    }

    return (
        <>
            <AddStock amount={props.amount} portfolio={props.portfolio} setGetUpdate={setGetUpdate} />
            <div className="stock-table">
                <Table variant="light" responsive className="table" striped={true}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Symbol</th>
                            <th># Shares</th>
                            <th>Initial Buy Price</th>
                            <th>Current Price</th>
                            <th>Total Spent</th>
                            <th>Unrealized Gains / Losses</th>
                            <th>Buy Data / Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {stocks.slice(page, page + ROWS_PER_PAGE).map((stock, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{stock.symbol}</td>
                                    <td>{stock.shares}</td>
                                    <td>${stock.buy_price}</td>
                                    <td>${prices.get(stock.symbol)}</td>
                                    <td>${(stock.total_buy_price * 1.0).toFixed(2)}</td>
                                    {
                                        (stock.total_buy_price > (stock.shares * prices.get(stock.symbol)))
                                            ?
                                            <td style={{ color: "red" }}>-${(stock.total_buy_price - stock.shares * prices.get(stock.symbol) * 1.0).toFixed(2)}</td>
                                            :
                                            <td style={{ color: "green" }}>${(stock.shares * prices.get(stock.symbol) - stock.total_buy_price * 1.0).toFixed(2)}</td>
                                    }
                                    <td>{stock.buydatetime}</td>
                                    <td>
                                        <SellStock stock={stock} amount={props.amount} current={prices.get(stock.symbol)}
                                            setGetUpdate={setGetUpdate} portfolio={props.portfolio} />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
            <div className='pagination-btns'>
                <Button variant='secondary' onClick={() => paginate('prev')}>Prev</Button>
                <Button variant='secondary' onClick={() => paginate('next')}>Next</Button>
            </div>
        </>
    );
}