import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import userService from '../services/user.service';
import stockService from '../services/stock.service';
import transactionService from '../services/transaction.service';
import portfolioService from '../services/portfolio.service';
import { Button, Modal, Form } from 'react-bootstrap';

export function SellStock(props) {
    const [show, setShow] = useState(false);
    const [shares, setShares] = useState(0);
    const [totalPrice, setTotalPrice] = useState(0);


    const handleModalClose = () => {
        setShow(false)
    }

    const handleModalShow = () => {
        setShow(true)
    }

    function updatePortfolio() {
        const pInfo = { "id": props.portfolio.id, "user_id": window.localStorage.getItem("loggedIn"), "available_amount": props.portfolio.available_amount + totalPrice, "name": props.portfolio.name, "amount": props.portfolio.amount }
        portfolioService.updatePortfolio(pInfo)
            .then(response => {
                props.setGetUpdate(new Date().getMilliseconds())
            })
            .catch(error => {
            })
    }

    function handleSharesChange(e) {
        if (e.target.value <= props.stock.shares && !isNaN(e.target.value) && e.target.value >= 0) {
            setShares(e.target.value)
            setTotalPrice(props.current * e.target.value)
        } else if (e.target.value == "") {
            setShares(0);
        } else {
            setShares(props.stock.shares)
            setTotalPrice(props.current * props.stock.shares)
        }
    }

    function saveChanges(e) {
        handleModalClose();
        if (shares > 0) {
            if (shares == props.stock.shares) {
                // delete 
                //props.setAll(true)
                stockService.sellAllStock(props.stock.id)
                    .then(response => {
                        // console.log("data added successfully", response.data);
                        let currentdate = new Date().toLocaleString();
                        const tInfo = { "action": "SELL", "datetime": currentdate, "symbol": props.stock.symbol, "amount": parseFloat(shares), "price": props.current, "portfolio_id": props.portfolio.id, "user_id": window.localStorage.getItem("loggedIn") }
                        transactionService.saveTransaction(tInfo)
                            .then(res => {
                                // console.log('success', res);
                                updatePortfolio();
                            })
                            .catch(err => { })
                    })
            } else {
                // update (put)
                //props.setAll(false)
                const info = { "id": props.stock.id, "buy_price": props.stock.buy_price, "buydatetime": props.stock.buydatetime, "shares": parseFloat(props.stock.shares) - parseFloat(shares), "symbol": props.stock.symbol, "total_buy_price": props.stock.buy_price * (props.stock.shares - shares), "user_id": window.localStorage.getItem("loggedIn"), "portfolio_id": props.portfolio.id }
                stockService.sellStock(info)
                    .then(response => {
                        // console.log("data added successfully", response.data);
                        let currentdate = new Date().toLocaleString();
                        const tInfo = { "action": "SELL", "datetime": currentdate, "symbol": props.stock.symbol, "amount": parseFloat(shares), "price": props.current, "portfolio_id": props.portfolio.id, "user_id": window.localStorage.getItem("loggedIn") }
                        transactionService.saveTransaction(tInfo)
                            .then(res => {
                                // console.log('success', res);
                                updatePortfolio();
                            })
                            .catch(err => { })
                    })
                    .catch(error => { })
            }
            setTotalPrice(0);
            setShares(0);
        }
    }

    return (
        <>
            <Button variant="success" onClick={handleModalShow}>Sell</Button>

            <Modal show={show} onHide={handleModalClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Sell Shares</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <h4>{props.stock.symbol}</h4>
                        <Form.Group className="mb-3" controlId="formShares">
                            <a target="_blank" rel="noopener noreferrer" href={"https://stocks-watch.herokuapp.com/symbol/" + props.stock.symbol}>View Stock Information</a>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formShares">
                            <Form.Label>Number of shares</Form.Label>
                            <Form.Control type="text" placeholder="Enter number of shares" value={shares} onChange={handleSharesChange} maxLength={10} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formShares">
                            <Form.Label>Current price: ${props.current}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formShares">
                            <Form.Label>Total cost: ${totalPrice}</Form.Label>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleModalClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={saveChanges}>
                        Sell Shares
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}