import { useEffect, useState } from 'react';
import { navigate} from 'react-router-dom';
import userService from '../services/user.service';
import { Button, Image } from 'react-bootstrap';


export function Logout(props) {

    const [user, setUser] = useState({});

    function handleSignOut(e) {
        setUser({});
        window.localStorage.removeItem("loggedIn")
        window.localStorage.removeItem("email")
        props.setUser({})
    }

    return (
        <div className="signout-btn-container">
            <Button variant='dark' onClick={(e) => handleSignOut(e)}>Sign Out</Button>
        </div>
    );
}