import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import userService from '../services/user.service';
import stockService from '../services/stock.service';
import transactionService from '../services/transaction.service'
import { Button, Modal, Form, Table, Pagination } from 'react-bootstrap';

export function TransactionsTable(props) {
    const [transactions, setTransactions] = useState([]);
    const [page, setPageIndex] = useState(0);

    let ROWS_PER_PAGE = 10;

    // console.log(props);

    useEffect(() => {
        transactionService.getUserTransactions(props.user_id, props.portfolio_id)
            .then(response => {
                // console.log(response.data);
                setTransactions(response.data.reverse());
            })
            .catch(error => {
                console.log('something went wrong', error);
            })
    }, [props])

    function paginate(params) {
        if (params === 'next') {
            if (page + ROWS_PER_PAGE > transactions.length) {
                setPageIndex(transactions.length - (transactions.length % ROWS_PER_PAGE));
            }
            else if (transactions.length == page + ROWS_PER_PAGE) {
                // Do nothing
            } else {
                setPageIndex(page + ROWS_PER_PAGE)
            }
        } else {
            if (page - ROWS_PER_PAGE >= 0) {
                setPageIndex(page - ROWS_PER_PAGE)
            }
        }
    }

    return (
        <>
            <h2>Transactions</h2>
            <div className="stock-table">
                <Table variant="light" responsive className="table" striped={true}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Symbol</th>
                            <th># Shares</th>
                            <th>Price</th>
                            <th>Total Price</th>
                            <th>Date & Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        {transactions.slice(page, page + ROWS_PER_PAGE).map((transaction, index) => {
                            return (
                                <tr key={index}>
                                    <td>{page + index + 1}</td>
                                    <td>{transaction.action}</td>
                                    <td>{transaction.symbol}</td>
                                    <td>{transaction.amount}</td>
                                    <td>${(transaction.price * 1.0).toFixed(2)}</td>
                                    <td>${(transaction.price * transaction.amount * 1.0).toFixed(2)}</td>
                                    <td>{transaction.datetime}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
            <div className='pagination-btns'>
                <Button variant='secondary' onClick={() => paginate('prev')}>Prev</Button>
                <Button variant='secondary' onClick={() => paginate('next')}>Next</Button>
            </div>
        </>
    );
}