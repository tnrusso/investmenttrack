import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Table, Image, Form } from 'react-bootstrap';
import userService from '../services/user.service';
import logo from '../images/logo.png'

export function Login(props) {
    const [user, setUser] = useState("");
    const [pass, setPass] = useState("");
    const [errorMessage, setErrorMessage] = useState(false);

    let navigate = useNavigate();

    function register() {
        navigate("/register")
    }

    function handleUser(e) {
        setUser(e.target.value);
    }

    function handlePass(e) {
        setPass(e.target.value);
    }

    function handleSubmit() {
        userService.get(user, pass)
            .then(response => {
                // console.log(response.data);
                if (response.data) {
                    // console.log("Login suceed")
                    window.localStorage.setItem("loggedIn", response.data.id)
                    setErrorMessage(false);
                    navigate("/")
                } else {
                    // Login Failed
                    setErrorMessage(true);
                }
            })
            .catch(error => {
                // console.log('something went wrong', error);
                setErrorMessage(true);
            })
    }

    const popover = (
        <Popover >
            <Popover.Body>
                Use these login details for a demonstration!
                <br /><br />
                <strong>User ID: </strong> user123
                <br />
                <strong>Password: </strong> pass123
            </Popover.Body>
        </Popover>
    );

    return (
        <div className="sign-in-container">
            <div className='sign-in-box'>
                <Image src={logo} fluid={true} onClick={() => navigate("/")} />
                <br />
                <Form>
                    <h1>Login</h1>
                    <Form.Group className="mb-3" controlId="userid">
                        <Form.Label>User ID</Form.Label>
                        <Form.Control type="text" placeholder="Enter User ID" onChange={handleUser} />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={handlePass} />
                    </Form.Group>
                    {(errorMessage
                        ?
                        <p className='error-message'>Invalid username or password</p>
                        :
                        <></>
                    )}
                    <span className='even-btns'>
                        <Button variant='primary' onClick={handleSubmit}>Login</Button>
                        <Button variant='success' onClick={register}>Register</Button>
                    </span>
                </Form>
                <br />
                <OverlayTrigger trigger="click" overlay={popover} placement={'bottom'}>
                    <Button variant="info">Don't want to register?</Button>
                </OverlayTrigger>
            </div>
        </div>
    );
}