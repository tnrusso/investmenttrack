import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import userService from '../services/user.service';
import portfolioService from '../services/portfolio.service';
import { Button, Modal, Form } from 'react-bootstrap';

export function CreatePortfolio(props) {
    const [portfolioName, setPortfolioName] = useState("");
    const [amount, setAmount] = useState(0);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function handleChange(e) {
        setPortfolioName(e.target.value);
    }

    function handleAmountChange(e) {
        if (!isNaN(e.target.value)) {
            setAmount(e.target.value)
        }
    }

    async function create() {
        if (portfolioName != "" && amount > 0) {
            // console.log(portfolioName)
            const info = { "name": portfolioName, "amount": amount, "user_id": window.localStorage.getItem("loggedIn"), "available_amount": amount}
            portfolioService.savePortfolio(info)
                .then(response => {
                    // console.log("data added successfully", response.data);
                    props.setNewPortfolio(response.data);
                })
                .catch(error => {
                    // console.log('something went wroing', error);
                })

            handleClose();
        }
    }

    return (
        <>
            <button className='create-portfolio-btn' onClick={handleShow}>
                <h6>Create New Portfolio</h6>
            </button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add New Portfolio</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formSymbol">
                            <Form.Label>Enter a name for your portfolio:</Form.Label>
                            <Form.Control type="text" placeholder="Enter portfolio name" value={portfolioName} onChange={handleChange} />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formSymbol">
                            <Form.Label>How much do you want to invest?</Form.Label>
                            <Form.Control type="text" placeholder="Enter investment amount" value={amount} onChange={handleAmountChange} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={create}>
                        Create
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}