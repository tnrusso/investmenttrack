package com.example.investtrack.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.investtrack.entity.Stock;
import com.example.investtrack.repository.StockRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class StockController {

    @Autowired
    private StockRepository eRepo;

    @GetMapping("/stock")
    public List<Stock> getAllStock() {
        return eRepo.findAll();
    }

    @GetMapping("/stock/{symbol}")
    public String getStockById(@PathVariable String symbol) throws UnsupportedEncodingException, IOException {
        URL url = new URL("https://finnhub.io/api/v1/quote?symbol=" + symbol + "&token=c8u97v2ad3ibddueeblg");
        String x = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            for (String line; (line = reader.readLine()) != null;) {
                x += line;
                System.out.println(line);
            }
        }
        return x;
    }

    @GetMapping("/userStocks/{user_id}/{portfolio_id}")
    public List<Stock> getUserStocksById(@PathVariable("user_id") Long user_id,
            @PathVariable("portfolio_id") Long portfolio_id) {
        return eRepo.findAllStocksById(user_id, portfolio_id);
    }

    @PostMapping("/saveStock")
    public Stock saveUserDetails(@RequestBody Stock stock) {
        return eRepo.save(stock);
    }

    @PutMapping("/sellStock")
    public Stock updateStockDetails(@RequestBody Stock stock) {
        return eRepo.save(stock);
    }

    @DeleteMapping("/sellAllStock/{id}")
    public ResponseEntity<HttpStatus> deleteStockById(@PathVariable("id") Long id) {
        eRepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}