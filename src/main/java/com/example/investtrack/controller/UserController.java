package com.example.investtrack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.investtrack.entity.User;
import com.example.investtrack.repository.UserRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserRepository eRepo;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return eRepo.findAll();
    }

    // @GetMapping("/users/{id}")
    // public User getUserById(@PathVariable Long id) {
    // return eRepo.findById(id).get();
    // }

    @GetMapping("/login/{name}/{pass}")
    public User getUserByNameAndPassword(@PathVariable("name") String name, @PathVariable("pass") String pass) {
        return eRepo.findByNameAndPassword(name, pass);
    }

    @PostMapping("/users")
    public User saveUserDetails(@RequestBody User user) {
        return eRepo.save(user);
    }

    @PutMapping("/users")
    public User updateUser(@RequestBody User user) {
        return eRepo.save(user);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<HttpStatus> deleteUserById(@PathVariable Long id) {
        eRepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}