package com.example.investtrack.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.investtrack.entity.Portfolio;
import com.example.investtrack.repository.PortfolioRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class PortfolioController {

    @Autowired
    private PortfolioRepository eRepo;

    @GetMapping("/portfolio/{id}")
    public Portfolio getPortfolioById(@PathVariable Long id) {
        return eRepo.findById(id).get();
    }

    @GetMapping("/userPortfolios/{user_id}")
    public List<Portfolio> getUserPortfoliosById(@PathVariable("user_id") Long user_id) {
        return eRepo.findAllPortfoliosById(user_id);
    }

    @PostMapping("/savePortfolio")
    public Portfolio saveUserDetails(@RequestBody Portfolio data) {
        return eRepo.save(data);
    }

    @PutMapping("/updatePortfolio")
    public Portfolio updateStockDetails(@RequestBody Portfolio data) {
        return eRepo.save(data);
    }

}