package com.example.investtrack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.investtrack.entity.Transaction;
import com.example.investtrack.repository.TransactionRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class TransactionController {

    @Autowired
    private TransactionRepository eRepo;

    @GetMapping("/userTransactions/{user_id}/{portfolio_id}")
    public List<Transaction> getUserTransactionsById(@PathVariable("user_id") Long user_id,
            @PathVariable("portfolio_id") Long portfolio_id) {
        return eRepo.findAllTransactionsById(user_id, portfolio_id);
    }

    @PostMapping("/saveTransaction")
    public Transaction saveUserDetails(@RequestBody Transaction transaction) {
        return eRepo.save(transaction);
    }

}
