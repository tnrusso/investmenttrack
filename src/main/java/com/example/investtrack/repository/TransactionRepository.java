package com.example.investtrack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.investtrack.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query(value = "SELECT * FROM transactions WHERE user_id = ?1 and portfolio_id = ?2 order by datetime asc", nativeQuery = true)
    List<Transaction> findAllTransactionsById(Long user_id, Long portfolio_id);
}