package com.example.investtrack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.investtrack.entity.Portfolio;

@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {
    @Query(value = "SELECT * FROM portfolios WHERE user_id = ?1 order by id asc", nativeQuery = true)
    List<Portfolio> findAllPortfoliosById(Long user_id);
}