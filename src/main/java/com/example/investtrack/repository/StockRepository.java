package com.example.investtrack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.investtrack.entity.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
    @Query(value = "SELECT * FROM stocks WHERE user_id = ?1 and portfolio_id = ?2 order by id asc", nativeQuery = true)
    List<Stock> findAllStocksById(Long user_id, Long portfolio_id);
}