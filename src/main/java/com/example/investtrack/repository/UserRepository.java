package com.example.investtrack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.investtrack.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "SELECT * FROM users where name = ?1 AND password = ?2", nativeQuery = true)
    User findByNameAndPassword(String name, String pass);
}